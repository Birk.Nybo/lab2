package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    ArrayList<FridgeItem> fridgeArrayList = new ArrayList<FridgeItem>(20);

    int maxSize = 20;

    
    @Override
    public int nItemsInFridge() {
        int itemCount = 0;
        for (int i = 0; i < fridgeArrayList.size(); i++) {
            if (fridgeArrayList.get(i) != null) {
                itemCount++;
            }
        }
        return itemCount;
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < maxSize) {
            fridgeArrayList.add(item);
            return true;
        } 
        return false;                  
         
         
    }

    @Override
    public void takeOut(FridgeItem item) {
        if(!fridgeArrayList.contains(item)) {
            throw new NoSuchElementException("The item does not exist");
        } fridgeArrayList.remove(item);
        
    }

    @Override
    public void emptyFridge() {
        fridgeArrayList.clear();        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredfood = new ArrayList<>();
        for(int i = 0; i < nItemsInFridge(); i++) {
            FridgeItem item = fridgeArrayList.get(i);
            if(item.hasExpired()) {
                expiredfood.add(item);
            }
        }
        for(FridgeItem expFoodItem : expiredfood) {
            fridgeArrayList.remove(expFoodItem);
        }
        return expiredfood;
    }
    
}
